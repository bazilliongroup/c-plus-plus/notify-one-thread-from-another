#include <iostream>
#include <thread>
#include <mutex>
#include <list>
#include <string>
#include <condition_variable>

class Request 
{ 
public:

	void putJobID(int jobId)
	{
		jobId_ = jobId;
	}

	int getJobId()
	{
		return jobId_;
	}

private:
	int jobId_;
};

//http://en.cppreference.com/w/cpp/thread/condition_variable
//A simple job queue class; don't do this, use std::queue
template<typename T>
class JobQueue
{

public:
	JobQueue() {}
	~JobQueue() {}

	//The thread that intends to modify the variable has to
	//	1) acquire a std::mutex(typically via std::lock_guard)
	//	2) perform the modification while the lock is held
	//	3) execute notify_one or notify_all on the std::condition_variable(the lock does not need to be held for notification)
	void submitJob(const T& x)
	{
		std::unique_lock<std::mutex> guard(mutex_);
		list_.push_back(x);
		workToBeDone_.notify_one();
	}

	//Any thread that intends to wait on std::condition_variable has to
	//	1) acquire a std::unique_lock<std::mutex>, on the same mutex as used to protect the shared variable
	//	2) execute wait, wait_for, or wait_until.The wait operations atomically release the mutex and suspend the execution of the thread.
	//	3) When the condition variable is notified, a timeout expires, or a spurious wakeup occurs, the thread is awakened, and the mutex 
	//	is atomically reacquired.The thread should then check the condition and resume waiting if the wake up was spurious.
	T getJob()
	{
		std::unique_lock<std::mutex> guard(mutex_);
		workToBeDone_.wait(guard); // Wait until this condition is satisfied, then lock the mutex
		T tmp = list_.front();
		list_.pop_front();

		return(tmp);
	}

private:
	std::list<T> list_;
	std::mutex mutex_;
	std::condition_variable workToBeDone_;
};

JobQueue<Request> myJobQueue;

void boss()
{
	int i = 0;

	for (;;)
	{
		// Get the request from somewhere
		Request req;
		req.putJobID(i);
		myJobQueue.submitJob(req);

		i++;

		if (i >= 1000)
		{
			i = 0;
		}
	}
}

void worker1()
{
	for (;;)
	{
		std::cout << "\nworker 1 jobId = " << myJobQueue.getJob().getJobId() << std::endl;
		//Do something with the job...
	}
}

void worker2()
{
	for (;;)
	{
		std::cout << "\nworker 2 jobId = " << myJobQueue.getJob().getJobId() << std::endl;
		//Do something with the job...
	}
}

int main()
{
	std::thread thr1(boss);
	std::thread thr2(worker1);
	std::thread thr3(worker2);

	thr1.join();
	thr2.join();
	thr3.join();
}
